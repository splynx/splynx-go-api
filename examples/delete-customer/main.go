package main

import (
	"bitbucket.org/splynx/splynx-go-api/api"
	"bitbucket.org/splynx/splynx-go-api/consts"
	"log"
)

func main() {
	// set to true for enable debug mode
	//api.Debug = true

	client := api.NewClient("https://demo.splynx.com", consts.API_VERSION_2_0)

	const API_KEY = "77f6d95bfb944d497cb65dcc824deaf9"
	const API_SECRET = "d5b06a65a75e6ab82e208cd0292914eb"
	const SPLYNX_CUSTOMER_ID = 18

	err := client.LoginByApiKey(API_KEY, API_SECRET)
	if err != nil {
		log.Fatal("Fail to login: ", err)
		return
	}

	log.Println("Success login!")

	var response interface{}

	httpRes, err := client.Delete("admin/customers/customer", SPLYNX_CUSTOMER_ID, &response)
	if err != nil {
		log.Fatal("Received error: ", err)
	}

	log.Println("Status code:", httpRes.StatusCode)
	log.Println("Response", response)

	switch status := httpRes.StatusCode; status {
	case 204:
		log.Println("Customer", SPLYNX_CUSTOMER_ID, "successfully deleted")
	case 404:
		log.Println("Customer #", SPLYNX_CUSTOMER_ID, "not found")
	default:
		log.Println("Failed to delete customer")
		log.Println("Please make sure that a customer have `inactive` status")
	}
}
