package main

import (
	"bitbucket.org/splynx/splynx-go-api/api"
	"bitbucket.org/splynx/splynx-go-api/consts"
	"fmt"
	"log"
	"math/rand"
	"time"
)

func main() {
	// set to true for enable debug mode
	// api.Debug = true

	client := api.NewClient("https://demo.splynx.com", consts.API_VERSION_2_0)

	const API_KEY = "77f6d95bfb944d497cb65dcc824deaf9"
	const API_SECRET = "d5b06a65a75e6ab82e208cd0292914eb"

	err := client.LoginByApiKey(API_KEY, API_SECRET)
	if err != nil {
		log.Fatal("Fail to login: ", err)
	}

	log.Println("Success login!")

	// Create randomizer
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	randNumber := fmt.Sprintf("%v", r1.Intn(1000))

	var responseBody interface{}
	var data = make(map[string]interface{})
	data["login"] = "golang_user_" + randNumber
	data["status"] = "active"
	data["name"] = "Golang User #" + randNumber
	data["email"] = "golang@user.com"

	httpResponse, err := client.Post("admin/customers/customer", data, &responseBody)
	if err != nil {
		log.Fatal("Request error: ", err)
	}

	log.Println("GET status code:", httpResponse.StatusCode)
	log.Println("GET response:", responseBody)

	if httpResponse.StatusCode == 201 {
		m := responseBody.(map[string]interface{})
		customerId := m["id"].(float64)
		log.Println("Customer successfully created, id: " + fmt.Sprintf("%v", customerId))
	} else {
		log.Println("Fail to create a customer")
	}
}
