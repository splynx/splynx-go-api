package main

import (
	"bitbucket.org/splynx/splynx-go-api/api"
	"bitbucket.org/splynx/splynx-go-api/consts"
	"log"
)

func main() {
	// set to true for enable debug mode
	// api.Debug = true

	client := api.NewClient("https://demo.splynx.com", consts.API_VERSION_2_0)

	const API_KEY = "77f6d95bfb944d497cb65dcc824deaf9"
	const API_SECRET = "d5b06a65a75e6ab82e208cd0292914eb"
	const SPLYNX_CUSTOMER_ID = 182317

	err := client.LoginByApiKey(API_KEY, API_SECRET)
	if err != nil {
		log.Fatal("Fail to login: ", err)
		return
	}

	log.Println("Success login!")

	var response interface{}

	httpRes, err := client.Get("admin/customers/customer", SPLYNX_CUSTOMER_ID, &response)
	if err != nil {
		log.Fatal("GET error: ", err)
	}

	log.Println("Status code:", httpRes.StatusCode)

	if httpRes.StatusCode != 200 {
		log.Println("Fail to get a customer by id")
	}

	log.Println("Customer #", SPLYNX_CUSTOMER_ID, " success retrieved")

	customer := response.(map[string]interface{})

	for k, v := range customer {
		log.Println("\t", k, " = ", v)
	}
}
