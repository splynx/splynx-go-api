package main

import (
	"bitbucket.org/splynx/splynx-go-api/api"
	"bitbucket.org/splynx/splynx-go-api/consts"
	"log"
)

func main() {
	// set to true for enable debug mode
	// api.Debug = true

	client := api.NewClient("https://demo.splynx.com", consts.API_VERSION_2_0)

	const API_KEY = "77f6d95bfb944d497cb65dcc824deaf9"
	const API_SECRET = "d5b06a65a75e6ab82e208cd0292914eb"

	err := client.LoginByApiKey(API_KEY, API_SECRET)
	if err != nil {
		log.Fatal("Fail to login: ", err)
		return
	}

	log.Println("Success login!")

	var response interface{}

	httpRes, err := client.Get("admin/customers/customer", 0, &response)
	if err != nil {
		log.Fatal("GET error: ", err)
	}

	log.Println("GET status code:", httpRes.StatusCode)
	//log.Println("GET Headers:", httpRes.Header)

	arr := response.([]interface{})
	for _, v := range arr {
		customer := v.(map[string]interface{})
		log.Println("Customer:", customer["id"], " -> ", customer["name"])
		//log.Println("Customer:", customer["id"], " -> ", customer)
	}
}
