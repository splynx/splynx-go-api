package main

import (
	"bitbucket.org/splynx/splynx-go-api/api"
	"bitbucket.org/splynx/splynx-go-api/consts"
	"fmt"
	"log"
	"math/rand"
	"time"
)

func main() {
	// set to true for enable debug mode
	// api.Debug = true

	client := api.NewClient("https://demo.splynx.com", consts.API_VERSION_2_0)

	const API_KEY = "77f6d95bfb944d497cb65dcc824deaf9"
	const API_SECRET = "d5b06a65a75e6ab82e208cd0292914eb"
	const CUSTOMER_ID_FROM_SPLYNX = 182317

	err := client.LoginByApiKey(API_KEY, API_SECRET)
	if err != nil {
		log.Fatal("Fail to login: ", err)
	}

	log.Println("Success login!")

	// Create randomizer
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	randNumber := fmt.Sprintf("%v", r1.Intn(1000))

	var responseBody interface{}
	var data = make(map[string]interface{})
	data["name"] = "Updated Golang User #" + randNumber

	httpResponse, err := client.Put("admin/customers/customer", CUSTOMER_ID_FROM_SPLYNX, data, &responseBody)
	if err != nil {
		log.Fatal("Request error: ", err)
	}

	log.Println("GET status code:", httpResponse.StatusCode)

	if httpResponse.StatusCode == 202 {
		log.Println("Customer successfully updated")
	} else {
		log.Println("Fail to update a customer")
	}
}
