package consts

const (
	// API_VERSION_1_0 Deprecated and not support by this helper
	API_VERSION_1_0 = "1.0"
	API_VERSION_2_0 = "2.0"

	CONTENT_TYPE_APPLICATION_JSON = "application/json"
	CONTENT_TYPE_MULTIPART_FORM_DATA = "multipart/form-data"

	LOGIN_TYPE_API_KEY = "api_key"
	LOGIN_TYPE_ADMIN = "admin"
	LOGIN_TYPE_CUSTOMER = "customer"
	LOGIN_TYPE_SESSION = "session"

	HEADER_X_TOTAL_COUNT = "x-total-count"

	ENDPOINT_TOKENS = "admin/auth/tokens"
)