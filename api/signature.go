package api

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"strings"
)

func CreateSignature(key string, secret string, nonce string) string {
	payload := nonce + key
	h := hmac.New(sha256.New, []byte(secret))
	h.Write([]byte(payload))
	sha := hex.EncodeToString(h.Sum(nil))

	return strings.ToUpper(sha)
}
