package api

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
)

type RequestOpts struct {
	path string
	method string
	data interface{}
}

func makeUrl(c *Client, path string) string {
	fixedHost := strings.TrimRight(c.Host, "/")
	fixedPath := strings.Trim(path, "/")

	return strings.Join([]string{fixedHost, "/api/", c.Version, "/", fixedPath}, "")
}

func makeRequest(c *Client, opts *RequestOpts) (*http.Request, error) {
	Log("Prepared request body:", opts.data)

	body, err := json.Marshal(opts.data)
	if err != nil {
		return nil, err
	}

	var req *http.Request
	if opts.method == "POST" || opts.method == "PUT" {
		req, err = http.NewRequest(opts.method, makeUrl(c, opts.path), bytes.NewBuffer(body))
	} else {
		req, err = http.NewRequest(opts.method, makeUrl(c, opts.path), nil)
	}

	if err != nil {
		return nil, err
	}

	return req, nil
}

func sendRequest(c *Client, req *http.Request, responseData interface{}) (*http.Response, error) {
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "application/json; charset=utf-8")

	res, err := c.Http.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	bodyString := string(body)
	Log(req.Method, ", url: ", req.URL)
	Log("Request headers:", req.Header)
	Log("Response code: ", res.StatusCode)
	Log("Response body: ", bodyString)

	if bodyString != "" {
		err = json.Unmarshal(body, &responseData)
		if err != nil {
			return nil, err
		}
	}

	return res, nil
}
