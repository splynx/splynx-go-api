package api

import (
	"bitbucket.org/splynx/splynx-go-api/consts"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

type Client struct {
	Host string
	Version string
	Http *http.Client
	Sash string

	nonce int
	AccessToken string
	accessTokenExpiration float64
	refreshToken string
	refreshTokenExpiration float64
	permissions interface{}
}

func (client *Client) LoginByApiKey(key, secret string) error {
	var data = make(map[string]interface{})
	data["auth_type"] = consts.LOGIN_TYPE_API_KEY
	data["key"] = key
	data["signature"] = CreateSignature(key, secret, strconv.Itoa(client.nonce))
	data["nonce"] = client.nonce
	client.nonce++

	return client.login(data)
}

func (client *Client) LoginByAdmin(login, password string) error {
	var data = make(map[string]interface{})
	data["auth_type"] = consts.LOGIN_TYPE_ADMIN
	data["login"] = login
	data["password"] = password

	return client.login(data)
}

func (client *Client) LoginByCustomer(login, password string) error {
	var data = make(map[string]interface{})
	data["auth_type"] = consts.LOGIN_TYPE_CUSTOMER
	data["login"] = login
	data["password"] = password

	return client.login(data)
}

func (client *Client) login(data map[string]interface{}) error {
	opts := RequestOpts{
		path: consts.ENDPOINT_TOKENS,
		method: "POST",
		data: data,
	}

	req, err := makeRequest(client, &opts)
	if err != nil {
		return err
	}

	var response map[string]interface{}
	res, err := sendRequest(client, req, &response)

	if err != nil {
		return err
	}

	if res.StatusCode == 201 {
		client.setAuthData(response)
		return nil
	}

	e := response["error"].(map[string]interface{})
	apiError := e["message"].(string)
	apiCode := fmt.Sprintf("%v", e["code"].(float64))
	apiInternalCode := e["internal_code"].(string)

	return errors.New(strings.Join([]string{apiError, "Code: ", apiCode, ", internal: ", apiInternalCode}, ""))
}

func (client *Client) Get(path string, id int, res *interface{}) (*http.Response, error) {
	opts := RequestOpts{
		path: preparePath(path, id),
		method: "GET",
	}

	req, err := makeRequest(client, &opts)
	if err != nil {
		return nil, err
	}

	client.setAuthorizationHeader(req)

	httpRes, err := sendRequest(client, req, &res)
	if err != nil {
		return nil, err
	}

	return httpRes, nil
}

func (client *Client) Post(path string, data interface{}, res *interface{}) (*http.Response, error) {
	opts := RequestOpts{
		path: preparePath(path, 0),
		method: "POST",
		data: data,
	}

	req, err := makeRequest(client, &opts)
	if err != nil {
		return nil, err
	}
	client.setAuthorizationHeader(req)

	httpRes, err := sendRequest(client, req, &res)
	if err != nil {
		return nil, err
	}

	return httpRes, nil
}

func (client *Client) Put(path string, id int, data interface{}, res *interface{}) (*http.Response, error) {
	opts := RequestOpts{
		path: preparePath(path, id),
		method: "PUT",
		data: data,
	}

	req, err := makeRequest(client, &opts)
	if err != nil {
		return nil, err
	}
	client.setAuthorizationHeader(req)

	httpRes, err := sendRequest(client, req, &res)
	if err != nil {
		return nil, err
	}

	return httpRes, nil
}

func (client *Client) Delete(path string, id int, res *interface{}) (*http.Response, error) {
	opts := RequestOpts{
		path: preparePath(path, id),
		method: "DELETE",
	}

	req, err := makeRequest(client, &opts)
	if err != nil {
		return nil, err
	}

	client.setAuthorizationHeader(req)

	httpRes, err := sendRequest(client, req, &res)
	if err != nil {
		return nil, err
	}

	return httpRes, nil
}

func (client *Client) setAuthData(data map[string]interface{}) {
	client.AccessToken = data["access_token"].(string)
	client.accessTokenExpiration = data["access_token_expiration"].(float64)
	client.refreshToken = data["refresh_token"].(string)
	client.refreshTokenExpiration = data["refresh_token_expiration"].(float64)
	client.permissions = data["permissions"]
}

func (client *Client) setAuthorizationHeader(req *http.Request) {
	header := fmt.Sprintf("Splynx-EA (access_token=%s)", client.AccessToken)
	req.Header.Set("Authorization", header)
}

func preparePath(path string, id int) string {
	path = strings.Trim(path, "/")

	if id != 0 {
		return strings.Join([]string{path, strconv.Itoa(id)}, "/")
	}

	return path
}