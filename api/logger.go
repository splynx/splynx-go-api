package api

import "log"

var Debug bool = false

func Log(v ...interface{}) {
	if Debug {
		log.Println(v)
	}
}
