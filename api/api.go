package api

import (
	"bitbucket.org/splynx/splynx-go-api/consts"
	"net/http"
	"time"
)

func NewClient(host string, version string) *Client {
	if version == "" {
		version = consts.API_VERSION_2_0
	}

	return &Client{
		Host: host,
		Version: version,
		Http: &http.Client{
			Timeout: time.Minute,
		},
	}
}